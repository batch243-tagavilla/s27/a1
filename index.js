// Users
{
    "id": 1,
    "firstName": "John",
    "lastName": "Doe",
    "email": "johndoe@mail.com",
    "password": "john1234",
    "isAdmin": false,
    "mobileNo": "09237593671"
}

// Orders
{
    "id": 10,
    "userId": 1,
    "productID" : 25,
    "transactionDate": "08-15-2021",
    "status": "paid",
    "total": 1500
}

// Products
{
    "id": 25,
    "name": "Humidifier",
    "description": "Make your home smell fresh any time.",
    "price": 300,
    "stocks": 1286,
    "isActive": true,
}

/* Mock Data */
// Users
{
    "id": 2,
    "firstName": "Ramil",
    "lastName": "Tagavilla",
    "email": "ramiltagavilla@mail.com",
    "password": "ramil1234",
    "isAdmin": false,
    "mobileNo": "09271234567"
}

// Orders
{
    "id": 1,
    "userId": 2,
    "productID" : 30,
    "transactionDate": "11-24-2022",
    "status": "paid",
    "total": 800
}

// Products
{
    "id": 30,
    "name": "JBL Headphones",
    "description": "Enjoy your music and experience great quality sound more than what you expected.",
    "price": 800,
    "stocks": 567,
    "isActive": true,
}

